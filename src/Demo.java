import java.sql.*;

/**
 *
 * @author www.luv2code.com
 */
public class Demo {

    public static void main(String[] args) throws SQLException {

        Connection myConn = null;
        Statement myStmt = null;
        ResultSet myRs = null;

        String user = "root";
        String pass = "";
        
        String myTableName = "CREATE TABLE employes (" 
        + "idNo INT(64) NOT NULL ,"  
        + "initials VARCHAR(2)," 
        + "employesdate DATE,"  
        + "salary INT(64))";  
        try {
            // 1. Get a connection to database
           myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", user, pass);   
           Statement s = myConn.createStatement();
           int myResult = s.executeUpdate(myTableName);
 
            System.out.println("Table Created");
        
        
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            if (myRs != null) {
                myRs.close();
            }

            if (myStmt != null) {
                myStmt.close();
            }

            if (myConn != null) {
                myConn.close();
            }
        }
    }    

}
